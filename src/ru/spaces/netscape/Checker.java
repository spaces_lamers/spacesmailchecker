package ru.spaces.netscape;

import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Checker {

	private DefaultHttpClient httpclient = new DefaultHttpClient();
	private HttpPost httpost = new HttpPost("http://spaces.ru/mysite/");
	private UserInterface ui;
	private volatile String c = "";
	private Timer timer;

	public Checker(String url, UserInterface ui) {
		this.ui = ui;
		HttpPost login = new HttpPost(url);
		try {
			EntityUtils.consume(httpclient.execute(login).getEntity());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void checking(int interval) {
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				try {
					HttpResponse response = httpclient.execute(httpost);
					HttpEntity entity = response.getEntity();
					InputStream in = entity.getContent();
					Document doc = Jsoup.parse(in, "utf-8", "");
					Elements table = doc.getElementsByTag("td");
					Element div = doc.getElementById("navi");
					if (table.size() > 0) {
						for (Element td : table) {
							if (td.toString().contains("mail")) {
								doc = Jsoup.parse(td.toString());
								Elements span = doc.getElementsByTag("span");
								if (span.size() > 0) {
									if (!c.equals(span.get(0).text())) {
										c = span.get(0).text();
										ui.showNotification(c);
									}
								}
								break;
							}
						}
					} else if (div != null) {
						doc = Jsoup.parse(div.toString());
						Elements links = doc.getElementsByTag("a");
						if (links.size() > 1) {
							for (Element link : links) {
								if (link.toString().contains("mail")) {
									Pattern p = Pattern.compile("\\d+");
									Matcher m = p.matcher(link.text());
									while (m.find()) {
										if (!c.equals(m.group())) {
											c = m.group();
											ui.showNotification(c);
										}
									}
									break;
								}
							}
						} else {
							ui.setWrongState();
						}
					} else {
						ui.setWrongState();
					}
				} catch (ClientProtocolException e) {
					ui.setWrongState();
				} catch (IOException e) {
					ui.setWrongState();
				}
			}
		}, 0, interval * 60000);
	}

	public void close() {
		timer.cancel();
		httpclient.getConnectionManager().shutdown();
		System.exit(0);
	}

	public void terminate() {
		timer.cancel();
	}

}
