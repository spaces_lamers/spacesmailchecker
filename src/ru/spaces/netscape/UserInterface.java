package ru.spaces.netscape;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;

public class UserInterface {

	private UserInterface ui;
	private Checker checker;
	private TrayIcon trayIcon;
	private Image image;
	private Preferences prefs;
	private JRadioButtonMenuItem rbMenuItem;
	private JMenu displayMenu;
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new UserInterface();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public UserInterface() {
		if (!SystemTray.isSupported()) {
			System.exit(0);
		}
		ui = this;

		image = Toolkit.getDefaultToolkit().getImage("res/mail_off.png");

		final SystemTray tray = SystemTray.getSystemTray();
		final JPopupMenu popUp = new JPopupMenu();

		JMenuItem exitItem = new JMenuItem("�����", new ImageIcon(
				"res/exit.png"));
		JMenuItem disconnectItem = new JMenuItem("�����������", new ImageIcon(
				"res/man_off.gif"));
		JMenuItem connectItem = new JMenuItem("������������", new ImageIcon(
				"res/man_on.gif"));
		displayMenu = new JMenu("��������");

		Icon atIcon = new ImageIcon("res/clock.png");
		displayMenu.setIcon(atIcon);

		displayMenu.setEnabled(false);

		ButtonGroup group = new ButtonGroup();

		rbMenuItem = new JRadioButtonMenuItem("1 ���", true);
		rbMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checker.terminate();
				checker.checking(1);
			}
		});
		group.add(rbMenuItem);
		displayMenu.add(rbMenuItem);

		rbMenuItem = new JRadioButtonMenuItem("2 ���");
		rbMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checker.terminate();
				checker.checking(2);
			}
		});
		group.add(rbMenuItem);
		displayMenu.add(rbMenuItem);

		rbMenuItem = new JRadioButtonMenuItem("5 ���");
		rbMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checker.terminate();
				checker.checking(5);
			}
		});
		group.add(rbMenuItem);
		displayMenu.add(rbMenuItem);

		rbMenuItem = new JRadioButtonMenuItem("10 ���");
		rbMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checker.terminate();
				checker.checking(10);
			}
		});
		group.add(rbMenuItem);
		displayMenu.add(rbMenuItem);

		rbMenuItem = new JRadioButtonMenuItem("30 ���");
		rbMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checker.terminate();
				checker.checking(30);
			}
		});
		group.add(rbMenuItem);
		displayMenu.add(rbMenuItem);

		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tray.remove(trayIcon);
				if (checker != null) {
					checker.close();
				}
			}
		});
		connectItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showInputDialog();
			}
		});

		disconnectItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setWrongState();
			}
		});

		popUp.add(connectItem);
		popUp.add(disconnectItem);
		popUp.addSeparator();
		popUp.add(displayMenu);
		popUp.add(exitItem);

		trayIcon = new TrayIcon(image, "Spaces mail checker", null);
		trayIcon.setImageAutoSize(true);

		trayIcon.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popUp.setLocation(e.getX(),
							e.getY() - popUp.getPreferredSize().height);
					popUp.setInvoker(popUp);
					popUp.setVisible(true);
				}
			}
		});

		try {
			tray.add(trayIcon);
		} catch (AWTException e1) {
			e1.printStackTrace();
		}
	}

	public void showInputDialog() {
		JTextField bookmark = new JTextField();
		JCheckBox save = new JCheckBox("���������");
		prefs = Preferences.userNodeForPackage(this.getClass());
		String login = prefs.get("LOGIN", "");
		bookmark.setText(login);
		save.setSelected(true);

		final JComponent[] inputs = new JComponent[] { bookmark, save };

		JOptionPane.showMessageDialog(null, inputs, "�����:",
				JOptionPane.PLAIN_MESSAGE);

		login = bookmark.getText();

		if (login.length() > 0) {
			if (save.isSelected()) {
				prefs.put("LOGIN", login);
			} else {
				prefs = Preferences.userNodeForPackage(this.getClass());
				try {
					prefs.remove("LOGIN");
				} catch (NullPointerException e) {
				}
			}
			checker = new Checker(login, ui);
			Image image_on = Toolkit.getDefaultToolkit().getImage(
					"res/mail_on.png");
			trayIcon.setImage(image_on);
			displayMenu.setEnabled(true);
			checker.checking(1);
		}
	}

	public void showNotification(String notif) {
		MessageBox window = new MessageBox();
		window.setBackground(Color.BLACK);
		window.setCounter(notif);
		window.setVisible(true);
		/*try {
			Sequence sequence = MidiSystem
					.getSequence(new File("C://beep.mid"));
			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			sequencer.setSequence(sequence);
			sequencer.start();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}*/
		for (int i = 0; i < screenSize.height / 2 - screenSize.height / 10; i++) {
			window.setLocation(screenSize.width / 2 - screenSize.width / 10, i);
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void setWrongState() {
		checker.terminate();
		trayIcon.setImage(image);
	}

}
