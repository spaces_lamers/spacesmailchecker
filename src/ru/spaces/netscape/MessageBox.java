package ru.spaces.netscape;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MessageBox extends JDialog {

	private static final long serialVersionUID = 1L;
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private JPanel contentPane;
	private JPanel main;
	private Point initialClick;
	private JLabel label1;
	private boolean b = true;

	public MessageBox() {
		this(null, null);
	}

	public MessageBox(String title) {
		this(null, title);
	}

	public MessageBox(JFrame frame) {
		this(frame, null);
	}

	public MessageBox(JFrame frame, String title) {
		super(frame, title);
		setTitle(title);
		initialize();
		showWindow();
		installListeners();
	}

	private void initialize() {
		setUndecorated(true);
		setSize(new Dimension(screenSize.width / 5, screenSize.height / 20));
		setLocationRelativeTo(null);
		setBackground(Color.BLACK);
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsDevice gd = ge.getDefaultScreenDevice();

		if (!gd.isWindowTranslucencySupported(java.awt.GraphicsDevice.WindowTranslucency.TRANSLUCENT)) {
			b = false;
		}
		if (b) {
			setOpacity(0.5f);
		}
		contentPane = new JPanel();
		setContentPane(contentPane);

		if (main == null) {
			main = new JPanel();
		}

		initialClick = new Point();
	}

	public JPanel getContentPane() {
		return main;
	}

	public void setLayout(LayoutManager manager) {
		if (main == null) {
			main = new JPanel();
			main.setLayout(new FlowLayout());
		} else {
			main.setLayout(manager);
		}

		JLabel label = new JLabel("New messages: ");
		label.setForeground(Color.WHITE);
		main.add(label);

		label1 = new JLabel();
		label1.setForeground(Color.WHITE);
		main.add(label1);

		if (!(getLayout() instanceof BorderLayout)) {
			super.setRootPaneCheckingEnabled(false);
			super.setLayout(new BorderLayout());
			super.setRootPane(super.getRootPane());
			super.setRootPaneCheckingEnabled(true);
		}
	}

	public void setBackground(Color color) {
		super.setBackground(color);

		if (contentPane != null) {
			contentPane.setBackground(color);
			main.setBackground(color);
		}
	}

	public Component add(Component comp) {
		return main.add(comp);
	}

	private void showWindow() {
		if (main.getLayout() == null) {
			setLayout(new FlowLayout());
		}
		contentPane.setLayout(new BorderLayout());
		contentPane.add(main, BorderLayout.CENTER);
		contentPane.setBorder(BorderFactory.createRaisedBevelBorder());
		setAlwaysOnTop(true);
	}

	private void installListeners() {
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				initialClick = e.getPoint();
				getComponentAt(initialClick);
			}
		});

		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				int thisX = getLocation().x;
				int thisY = getLocation().y;
				int xMoved = (thisX + e.getX()) - (thisX + initialClick.x);
				int yMoved = (thisY + e.getY()) - (thisY + initialClick.y);
				int X = thisX + xMoved;
				int Y = thisY + yMoved;
				setLocation(X, Y);
			}
		});

		contentPane.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				close();
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseExited(MouseEvent e) {
			}
		});
	}

	public void close() {
		setVisible(false);
		dispose();
	}

	public void setCounter(String arg0) {
		label1.setText(arg0);
	}

}
